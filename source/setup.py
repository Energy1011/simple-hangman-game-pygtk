from distutils.core import setup
import py2exe
import sys

# Find GTK+ installation path
__import__('gtk')
m = sys.modules['gtk']
gtk_base_path = m.__path__[0]

setup(
    name = 'Simple-Hangman-Game',
    description = 'Simple hangman game written in python with gtk+',
    version = '1.0',

    windows = [
                  {
                      'script': 'hangman.py'
                  }
              ],

    options = {
                  'py2exe': {
                      'packages':'encodings',
                      'includes': 'cairo, pango, pangocairo, atk, gobject, gio, gtk.keysyms, rsvg'
                  }
              }
)