#!/usr/bin/env python
# coding=utf-8
# graphs.py
# This file is part of Prácticas Python Hangman
#
# Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
#
# Prácticas Python Hangman is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Prácticas Python Hangman is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Prácticas Python Hangman; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, 
# Boston, MA  02110-1301  USA
#

# Hangman game

# this is to draw a simple hangman
graph_hangman_ascii = [
"""
|--------|
|        |
|
|
|
|
|
----------
""",
"""
|--------|
|        |
|        O
|
|
|
|
----------
""",
"""
|--------|
|        |
|        O
|        |
|
|
|
----------
""",
"""
|--------|
|        |
|        O
|       /|
|
|
|
----------
""",
"""
|--------|
|        |
|        O
|       /|\\
|
|
|
----------
""",
"""
|--------|
|        |
|        O
|       /|\\
|        |
|
|
----------
""",
"""
|--------|
|        |
|        O
|       /|\\
|        |
|       /
|
----------
""",
"""
|--------|
|        |
|        O Aahhgg !!!
|       /|\\
|        |
|       / \\
|
----------
""",
"""
|-----|
|        
|       :D I love you for saving my life !
|       /|\\
|        |
|       / \\
----------
"""]
