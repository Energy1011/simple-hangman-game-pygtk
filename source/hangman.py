#!/usr/bin/env python
# coding=utf-8
# hangman.py
# This file is part of Prácticas Python Hangman
#
# Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
#
# Prácticas Python Hangman is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Prácticas Python Hangman is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Prácticas Python Hangman; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA  02110-1301  USA
#

# Hangman game

import string
import random
# import the graphics to draw the hangman
from graphs import *
import pygtk
import gtk
pygtk.require('2.0')

class Game:

    flag_file_selected= False
    #Gtk widgets
    textview = gtk.TextView()
    textbuffer = textview.get_buffer()
    entry = None
    label_total_games = None
    label_total_wins = None
    label_total_losses = None
    label_actual_hit = None
    label_fail_attemps = None
    #other game vars
    flag_game_playing = False
    fail_attempts = 0
    total_games_played = 0
    wins = 0
    losses = 0
    actualHint = " "

    def gtk_main(self):
        gtk.main()

    def delete_event(self, widget, event, data=None):
        return False

    def destroy(self, widget, data=None):
        gtk.main_quit()

    def __init__(self,fail_attempts = 0,
                                the_secret_word_list=[],
                                the_hint_word_list=[],
                                actual_secret_word_playing=[],
                                hint_letters_revealed=[],
                                text_of_progress=[]):

        self.fail_attempts = fail_attempts
        self.the_secret_word_list = the_secret_word_list
        self.the_hint_word_list = the_hint_word_list
        self.actual_secret_word_playing = actual_secret_word_playing
        self.hint_letters_revealed = hint_letters_revealed
        self.text_of_progress = text_of_progress

        # Create a new window
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_size_request(400,400)
        window.set_title("Hangman Game")
        window.connect("delete_event", self.delete_event)
        window.connect("destroy", self.destroy)
        window.set_border_width(10)

        # Vbox
        vbox = gtk.VBox(False,0)
        window.add(vbox)

        #Status labelslabel_total_games
        string = "Played Games: %d" % self.total_games_played
        self.label_total_games = gtk.Label(string)
        vbox.pack_start(self.label_total_games, False, True, 0)

        string = "Wins: %d" % self.wins
        self.label_total_wins = gtk.Label(string)
        vbox.pack_start(self.label_total_wins, False, True, 0)

        string = "Losses: %d" % self.losses
        self.label_total_losses = gtk.Label(string)
        vbox.pack_start(self.label_total_losses, False, True, 0)

        string = "Strikes: %s of 7" % self.fail_attempts
        self.label_fail_attemps = gtk.Label(string)
        vbox.pack_start(self.label_fail_attemps, False, True, 0)

        string = "Hint: %s" % self.actualHint
        self.label_actual_hit = gtk.Label(string)
        self.label_actual_hit.set_line_wrap(True)
        vbox.pack_start(self.label_actual_hit, True, True, 0)


        # Textarea
        self.textview.set_editable(False)
        sw = gtk.ScrolledWindow()
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        sw.add(self.textview)
        vbox.pack_start(sw, True, True, 0)
        self.textview.set_cursor_visible(False)

        # Label
        label = gtk.Label("Try one letter")
        vbox.pack_start(label, False, True,0)

        # Entry
        self.entry = gtk.Entry(1)
        vbox.pack_start(self.entry, False, True, 0)
        self.entry.connect("activate", self.do_user_attempt, self.entry)

        # Buttons
        button = gtk.Button("Let's Play")
        button.connect("clicked", self.init_game)
        vbox.pack_start(button, True, True, 0)

        # Show all window
        window.show_all()

    def show_word_progress(self):
        # iter over the list of hints letter revealed
        for letter in self.hint_letters_revealed:
            # check for other letter existences in the entire word
            for e in xrange(0,len(self.actual_secret_word_playing)):
                if self.actual_secret_word_playing[e] == letter:
                    self.text_of_progress[e] = letter
                pass
            pass
        # print "Progress: %s" %self.text_of_progress
        string = "Progress: "
        string = string + ' '.join(str(e) for e in self.text_of_progress)
        string = string + ' '
        self.textbuffer.insert_at_cursor(string)


    def init_the_text_progress(self):
        for pos in list(self.actual_secret_word_playing):
            self.text_of_progress.append("_")

    def init_the_hint(self):
        random.seed()
        rand_num_letter = random.randint(0,len(self.actual_secret_word_playing)-2)
        hint_letter = self.actual_secret_word_playing[rand_num_letter]
        self.hint_letters_revealed.append(hint_letter)

    def generate_random_number(self):
        # set the seed to random
        random.seed()
        # generate rand number in range of 0 to the_secret_word_list len
        rand_num = random.randint(0,len(self.the_secret_word_list)-1)
        return rand_num

    def get_word_to_play_from_list(self):
        # set the actual word to play
        myrand = self.generate_random_number()
        self.actual_secret_word_playing = list(self.the_secret_word_list[myrand])
        self.actualHint = self.actualHint + ''.join(str(e) for e in self.the_hint_word_list[myrand])
        self.actualHint = self.actualHint.strip()
        # remove \n if exist
        if self.actual_secret_word_playing[-1] == '\n':
            self.actual_secret_word_playing.pop()
        # print "Secret word to play!! ->%s" %self.actual_secret_word_playing
        # self.actual_secret_word_playing.remove("\n")


    def do_user_attempt(self, widget, entry):
        if self.fail_attempts < 7 and self.flag_game_playing == True:
            user_try_letter = entry.get_text()
            try:
                try_it = self.actual_secret_word_playing.index(user_try_letter)
                self.hint_letters_revealed.append(user_try_letter)
                # print "Yeah nice attempt!"
            except:
                # print "Bad attempt"
                self.fail_attempts += 1
            self.draw_hangman(self.fail_attempts)
            self.show_word_progress()
            self.check_game_logic()
            self.entry.set_text('')
        self.update_status_game_labels()
        pass

    def draw_hangman(self,graph_index):
        self.textbuffer.set_text(graph_hangman_ascii[graph_index])

    def show_file_chooser(self):
        dialog = gtk.FileChooserDialog("Select a wordlist .txt file to play...",
                                        None,
                                        gtk.FILE_CHOOSER_ACTION_OPEN,
                                        (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN, gtk.RESPONSE_OK))

        dialog.set_default_response(gtk.RESPONSE_OK)
        filter = gtk.FileFilter()
        filter.set_name("Text")
        filter.add_pattern("*.txt")
        dialog.add_filter(filter)

        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.flag_file_selected = True
            res =  dialog.get_filename()
        elif response == gtk.RESPONSE_CANCEL:
            res =  False
        dialog.destroy()
        return res

    def read_words_from_file(self):
            path_file = self.show_file_chooser()
            if path_file <> False:
                try:
                    # open the file with the list of words to play
                    file_of_words=open(path_file,'r')
                    line=file_of_words.readline()
                    while line!="":
                        # get the next line
                        # hint exist ?
                        if line.find(" [") <> -1:
                            secret_string = line[0:line.find(" [")]
                        else :
                            secret_string = line

                        self.the_secret_word_list.append(secret_string)

                        if line.find("[") <> -1 or line.find("]") <> -1:
                            word_hint = line[line.find("[")+1:line.find("]")]
                        else:
                            word_hint = "No hint :("
                        self.the_hint_word_list.append(word_hint)
                        # add word line to the global list
                        line=file_of_words.readline()
                    # close the file
                    file_of_words.close()
                    return True
                except EOFError:
                    print "Error opening words.txt file"
                    return False
            else:
                return False

    def update_status_game_labels(self):
        string = "Played Games: %d" % self.total_games_played
        self.label_total_games.set_text(string)

        string = "Wins: %d" % self.wins
        self.label_total_wins.set_text(string)

        string = "Losses: %d" % self.losses
        self.label_total_losses.set_text(string)

        string = "Hint: %s" % self.actualHint
        self.label_actual_hit.set_text(string)

        string = "Strikes: %s of 7" % self.fail_attempts
        self.label_fail_attemps.set_text(string)

    def start_new_game_vars(self):
        self.fail_attempts = 0
        self.flag_game_playing = True
        self.text_of_progress = []
        self.hint_letters_revealed=[]
        self.actualHint = " "
        self.get_word_to_play_from_list()
        self.init_the_hint()
        self.init_the_text_progress()
        self.update_status_game_labels()
        self.draw_hangman(0)
        self.show_word_progress()

    # init the game
    def init_game(self, data):
        if self.flag_file_selected:
            self.start_new_game_vars()
        else:
            # print "We need words textfile to play"
            if self.read_words_from_file():
                 self.start_new_game_vars()


    def check_if_win_this_game(self):
        if self.actual_secret_word_playing == self.text_of_progress:
            self.draw_hangman(8)
            return True
        else:
            return False
            pass

    def check_game_logic(self):
        # When win
        if self.check_if_win_this_game() and self.flag_game_playing == True:
            self.wins +=1
            self.show_word_progress()
            self.flag_game_playing = False
        if self.fail_attempts == 7:
            self.losses +=1
            self.update_status_game_labels()
            self.flag_game_playing = False
        pass

# main function
def main():
    hangman = Game()
# init the game
    init = hangman.init_game(None)

    if init <> False:
    # main loop
            hangman.gtk_main()

if __name__ == "__main__":
    # call to main
    main()